/*
bluetooth source file
*/


#include "BL_driver.h"
#include <string.h>
#include <float.h>

//void BL_TransmitPressure(void){
//	sprintf(BleBuff, "Pres"); HAL_UART_Transmit(&huart1, BleBuff, 4, HAL_TIMEOUT);
//	sprintf(BleBuff, "sure"); HAL_UART_Transmit(&huart1, BleBuff, 4, HAL_TIMEOUT);
//	sprintf(BleBuff, " = "); HAL_UART_Transmit(&huart1, BleBuff, 4, HAL_TIMEOUT);
//	sprintf(BleBuff, "%f", PressureOut1); HAL_UART_Transmit(&huart1, BleBuff, sizeof(BleBuff), HAL_TIMEOUT);
//	sprintf(BleBuff, "\n"); HAL_UART_Transmit(&huart1, BleBuff,1, HAL_TIMEOUT);
//}

/*
 *	Transmits a key-value pair after converting
 *	it to the following format-
 *	"{key}:{value}\n"
*/
void BL_transmit(char key[], float value){
	sprintf(BleBuff, key); HAL_UART_Transmit(&huart1, BleBuff, strlen(key), HAL_TIMEOUT);
	int final = (int)(value*100);
	sprintf(BleBuff, ": %i \n",final);
	HAL_UART_Transmit(&huart1, BleBuff, strlen(BleBuff), HAL_TIMEOUT);
}

/*
 * Starts listening for a Bluetooth command under
 * interrupt mode. Receiving is complete when exactly
 * 7 bytes have been received.
*/
void BL_Receive(void){
	HAL_UART_Receive_IT(&huart1,BleReceiveBuff,7);
}

/*
bluetooth header file
*/


#include "main.h"

// UART instance handle
extern UART_HandleTypeDef huart1;

//UART Buffer for Transmitting
extern unsigned char BleBuff[50];

//UART Buffers for Receiving
extern unsigned char BleReceiveBuff[50];

//void BL_TransmitPressure(void);

/*
 *	Transmits a key-value pair after converting
 *	it to the following format-
 *	"{key}:{value}\n"
*/
void BL_transmit(char key[], float value);

/*
 * Starts listening for a Bluetooth command under
 * interrupt mode. Receiving is complete when exactly
 * 7 bytes have been received.
*/
void BL_Receive(void);

/*
.h file fot comtrolling leds


*/

#include "main.h"


#define A0_port	GPIOB
#define A0_pin	GPIO_PIN_7

#define A1_port	GPIOB
#define A1_pin	GPIO_PIN_6

#define A2_port	GPIOB
#define A2_pin	GPIO_PIN_8


#define LED1_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_RESET)

#define LED2_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_RESET)

#define LED3_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_RESET)

#define LED4_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_RESET)

#define LED5_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_SET)

#define LED6_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_SET)

#define LED7_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_RESET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_SET)

#define LED8_ON()	HAL_GPIO_WritePin(A0_port, A0_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A1_port, A1_pin, GPIO_PIN_SET);\
					HAL_GPIO_WritePin(A2_port, A2_pin, GPIO_PIN_SET)

